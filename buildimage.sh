log()
{
    echo -e "[`date '+%Y-%m-%d %T'`]:" $1
}

build_image()
{
    log "INFO: Starting docker build of $1"

    /usr/bin/docker build --build-arg BASE_IMAGE_NAME=${BASE_IMAGE} -t ${Build_Img}:${tag} . --no-cache --force-rm
    if [ $? -ne "0" ]; then
        log "ERROR: Docker build failed"
        exit
    fi

}

push_image()
{
    log "INFO: Login to the docker registry"
    echo $passwd | /usr/bin/docker login -u kishore4122 --password-stdin

    if [ $? -ne "0" ]; then
        log "ERROR: Docker registry login failed"
        exit 1
        else
        log "INFO: Docker registry login [[ success ]]"
    fi

    log "INFO: Starting to push image to the repository"
        /usr/bin/docker push ${Build_Img}:${tag}

    if [ $? -ne "0" ]; then
        log "ERROR: Docker push command failed"
        exit 1
        else
        log "INFO: Docker push to registry successed"
    fi
}



BASE_IMAGE=$1
Build_Img=$2
tag=$3
passwd=$4

build_image
push_image